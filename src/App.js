import React, {Component} from 'react';
import axios from 'axios';
import { Input, FormGroup, Label, Modal, ModalHeader, ModalBody, ModalFooter, Table, Button } from 'reactstrap';


class App extends Component {
  state = {
    products: [],
    newProductData:{
      name: '',
      price: '',
      category_id: ''
    },
    editProductData:{
      id: '',
      name: '',
      price: '',
      category_id: ''
    },
    newProductModal: false,
    editProductModal: false
  }
  componentWillMount(){
    this._refreshProducts();
    /*axios.get('http://localhost:5000/api/products').then((response) => {
      this.setState({
        products: response.data
      })
    });*/
  }
  toggleNewProductModal(){
    this.setState({
      newProductModal: ! this.state.newProductmodal
    });
  }
  toggleEditProductModal(){
    this.setState({
      editProductModal: ! this.state.editProductmodal
    });
  }
  addProduct() {
    axios.post('http://localhost:5000/api/products', this.state.newProductData).then((response) => {
      let { products } = this.state;
      products.push(response.data);
      this.setState({ products, newProductModal: false, newProductData: {
        name: '',
        price: '',
        category_id: ''
      }});
    });
  }
  updateProduct() {
    let {id, name, price, category_id} = this.state.editProductData;
    axios.put('http://localhost:5000/api/products/' + this.state.editProductData.id, {
      id, name, price, category_id
    }).then((response) => {
      this._refreshProducts();
      this.setState({
        editProductModal: false, editProductData: { id: '', name: '', price: '', category_id: ''}
      })
    });
  }
  editProduct(id, name, price, category_id){
    this.setState({
      editProductData: {id, name, price, category_id}, editProductModal: ! this.state.editProductModal
    });
  }
  deleteProduct(id) {
    axios.delete('http://localhost:5000/api/products/' + id).then((response) => {
      this._refreshProducts()
    });
  }
  _refreshProducts(){
    axios.get('http://localhost:5000/api/products').then((response) => {
      this.setState({
        products: response.data
      })
    });
  }
  render(){
    let products = this.state.products.map((product) => {
      return (
        <tr key={product.id}>
          <td>{product.id}</td>
          <td>{product.name}</td>
          <td>{product.price}</td>
          <td>{product.category_id}</td>
          <td>
            <Button color="success" size="sm" className="mr-2" onClick={this.editProduct.bind(this, product.id, product.name, product.price, product.category_id)}>Edit</Button>
            <Button color="danger" size="sm" onClick={this.deleteProduct.bind(this, product.id)} >Delete</Button>
          </td>
        </tr>
      )
    })
    return (
      <div className="App container">
        <h1> Sincosoft Store Products </h1>
        <Button className="my-3" color="primary" onClick={this.toggleNewProductModal.bind(this)}>Add a product</Button>
        <Modal isOpen={this.state.newProductModal} toggle={this.toggleNewProductModal.bind(this)}>
          <ModalHeader toggle={this.toggleNewProductModal.bind(this)}>Add a product</ModalHeader>
          <ModalBody>
            <FormGroup>
              <Label for="name">Name</Label>
              <Input id="name" value={this.state.newProductData.name} onChange={(e) => {
                let { newProductData } = this.state;
                newProductData.name = e.target.value;

                this.setState({newProductData});
              }} />
            </FormGroup>
            <FormGroup>
              <Label for="price">Price</Label>
              <Input id="price" value={this.state.newProductData.price} onChange={(e) => {
                let {newProductData} = this.state;
                newProductData.price = e.target.value;
                this.setState({newProductData});
              }} />
            </FormGroup>
            <FormGroup>
              <Label for="category_id">Category</Label>
              <Input id="category_id" value={this.state.newProductData.category_id} onChange={(e) => {
                let {newProductData} = this.state;
                newProductData.category_id = e.target.value;
                this.setState({newProductData});
              }}/>
            </FormGroup>
          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={this.addProduct.bind(this)}>Add product</Button>{' '}
            <Button color="secondary" onClick={this.toggleNewProductModal.bind(this)}>Cancel</Button>
          </ModalFooter>
        </Modal>

        <Modal isOpen={this.state.editProductModal} toggle={this.toggleEditProductModal.bind(this)}>
          <ModalHeader toggle={this.toggleEditProductModal.bind(this)}>Edit the product</ModalHeader>
          <ModalBody>
            <FormGroup>
              <Label for="name">Name</Label>
              <Input id="name" value={this.state.editProductData.name} onChange={(e) => {
                let { editProductData } = this.state;
                editProductData.name = e.target.value;

                this.setState({editProductData});
              }} />
            </FormGroup>
            <FormGroup>
              <Label for="price">Price</Label>
              <Input id="price" value={this.state.editProductData.price} onChange={(e) => {
                let {editProductData} = this.state;
                editProductData.price = e.target.value;
                this.setState({editProductData});
              }} />
            </FormGroup>
            <FormGroup>
              <Label for="category_id">Category</Label>
              <Input id="category_id" value={this.state.editProductData.category_id} onChange={(e) => {
                let {editProductData} = this.state;
                editProductData.category_id = e.target.value;
                this.setState({editProductData});
              }}/>
            </FormGroup>
          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={this.updateProduct.bind(this)}>Update product</Button>{' '}
            <Button color="secondary" onClick={this.toggleEditProductModal.bind(this)}>Cancel</Button>
          </ModalFooter>
        </Modal>

        <Table>
          <thead>
            <tr>
              <th>#</th>
              <th>Product</th>
              <th>Price</th>
              <th>Category</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            {products}
          </tbody>
        </Table>
      </div>
    );
  }
}

export default App;
